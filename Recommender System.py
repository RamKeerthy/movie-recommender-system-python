#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 27 01:12:05 2020

@author: ramkeerthyathinarayanan
"""

import pandas as pd
from datetime import datetime

similarity_score = 0.95
final_dataframe = pd.DataFrame(columns=['userId','recommendedMovie','rank'])
final_dataframe_tags = pd.DataFrame(columns=['userId','recommendedMovie','timeRank','tagRank'])

def ListDiff(li1, li2):
    return (list(set(li1) - set(li2)))

def timeAverage(batch_data):
    hour = 0
    minute = 0
    second = 0
    for index, user_row in batch_data.iterrows():
        split_string = user_row.datetime_timestamp.split(':')
        hour = hour + int(split_string[0])
        minute = minute + int(split_string[1])
        second = second + int(split_string[2])
    average_time = str(round(hour / len(batch_data))) + ':' + str(round(minute / len(batch_data))) + ':' + str(round(second / len(batch_data)))
    return average_time

def timeInSeconds(time):
    split_string = time.split(':')
    hour = int(split_string[0])
    minute = int(split_string[1])
    second = int(split_string[2])
    return (hour * 3600) + (minute * 60) + second

def getMovieIdForMovie(movie_name, data):
    movie_detail = data.loc[data['title'] == movie_name]
    return movie_detail['movieId']

def jaccard_similarity(list1, list2):
    intersection = len(list(set(list1).intersection(list2)))
    union = (len(list1) + len(list2)) - intersection
    return float(intersection) / union

movies = pd.read_csv("ml-latest-small/movies.csv")

movies = movies.loc[:,["movieId","title","genres"]]

ratings = pd.read_csv("ml-latest-small/ratings.csv")

ratings = ratings.loc[:,["userId","movieId","rating","timestamp"]]

datetime_timestamp = []

for index, rating in ratings.iterrows():
    datetime_timestamp.append(datetime.utcfromtimestamp(int(rating['timestamp'])).strftime('%H:%M:%S'))
    
ratings['datetime_timestamp'] = datetime_timestamp

data = pd.merge(movies,ratings)

data.shape

pivot_table = data.pivot_table(index = ["title"],columns = ["userId"],values = "rating")
pivot_table.head(10)

total_users = len(pivot_table.columns)

for index in range(total_users):
    
    user_id = index + 1
    print('Processing user ', user_id)

    user = pivot_table[user_id]
    user_movies = []
    
    # Append liked movies by the particular user taken into consideration
    for movie_rating in user.iteritems():
        if(movie_rating[1] >= 3):
            user_movies.append(movie_rating[0])
    
    # Finding similar users for the user taken into consideration and sorting them in descending order
    similarity_with_other_users = pivot_table.corrwith(user)
    similarity_with_other_users = similarity_with_other_users.sort_values(ascending=False)
    
    # Append all the movies from the similar users which has ratings greater than or equal to 3
    # At the same time the movies should not be present in the user list
    recommended_movie = []
        
    for similarity in similarity_with_other_users.iteritems():
        if(similarity[1] > similarity_score and similarity[1] < 1.0):
            similar_user = pivot_table[similarity[0]]
            
            for movie_rating in similar_user.iteritems():
                if(movie_rating[1] >= 3 and movie_rating[0] not in recommended_movie):
                    if(movie_rating[0] not in user_movies):
                        recommended_movie.append(movie_rating[0])
    
    # Finding time average of the user     
    user_data = data.loc[data['userId'] == user_id]
    user_time_average = timeAverage(user_data)
    
    # Finding time average for each and every recommended movie
    recommended_movie_dict = {}
    
    for movie in recommended_movie:
        movie_data = data.loc[data['title'] == movie]
        movie_time_average = timeAverage(movie_data)
        time_difference = abs(timeInSeconds(user_time_average) - timeInSeconds(movie_time_average))
        recommended_movie_dict[movie] = time_difference
    
    sorted_recommended_movie_dict = {k: v for k, v in sorted(recommended_movie_dict.items(), key=lambda item: item[1])}
    
    rank = 1
    for index, value in sorted_recommended_movie_dict.items():
        temp_dataframe = {'userId': user_id, 'recommendedMovie' : index, 'rank' : rank}
        final_dataframe = final_dataframe.append(temp_dataframe, ignore_index=True)
        rank = rank + 1

final_dataframe.to_csv('recommendatio.csv')

# Recommendation based on tags

tags = pd.read_csv("ml-latest-small/tags.csv")

tags = tags.loc[:,["userId", "movieId", "tag"]]

jaccard_table = pd.DataFrame(columns=['userId','recommendedMovie','timeRank','tagJaccard'])

for index in range(total_users):
    
    user_id = index + 1
    print('Processing jaccard of user ', user_id)
    
    tag_data = tags.loc[tags['userId'] == user_id]
    final_movie_data = final_dataframe.loc[final_dataframe['userId'] == user_id]
    
    user_tags = []
    for index, movie in tag_data.iterrows():
        user_tags.append(movie['tag'])
    
    if(len(user_tags) > 0):
        for index, row in final_movie_data.iterrows():
            movie_id = 0
            movie_id_list = getMovieIdForMovie(row['recommendedMovie'], data)
            
            if(len(movie_id_list) > 0):
                movie_id = movie_id_list.iloc[0]
            
            movie_tag_data = tags.loc[tags['movieId'] == movie_id]
            
            movie_tags = []
            for index, movie in movie_tag_data.iterrows():
                movie_tags.append(movie['tag'])
                
            jaccard = jaccard_similarity(user_tags, movie_tags)
            temp_dataframe = {'userId': row['userId'], 'recommendedMovie' : row['recommendedMovie'], 'timeRank' : row['rank'], 'tagJaccard': jaccard}
            jaccard_table = jaccard_table.append(temp_dataframe, ignore_index=True)
    else:
        for index, row in final_movie_data.iterrows():
            temp_dataframe = {'userId': row['userId'], 'recommendedMovie' : row['recommendedMovie'], 'timeRank' : row['rank'], 'tagJaccard': 0}
            jaccard_table = jaccard_table.append(temp_dataframe, ignore_index=True)

jaccard_table = jaccard_table.sort_values(['userId', 'tagJaccard', 'timeRank'], ascending=[True, False, True])    
jaccard_table.to_csv('recommendation_jaccard1.csv')

# Recommendation based on genre

recommendation_tag_genre_time = pd.DataFrame(columns=['userId','recommendedMovie','timeRank','tagJaccard','genreJaccard'])

for index in range(total_users):
    
    user_id = index + 1
    print('Processing genre jaccard of user ', user_id)
    
    genre_data = data.loc[data['userId'] == user_id]
    final_movie_data = jaccard_table.loc[jaccard_table['userId'] == user_id]
    
    user_genre = []
    for index, movie in genre_data.iterrows():
        split_genres = movie['genres'].split('|')
        for genre in split_genres:
            if(genre not in user_genre):
                user_genre.append(genre)
                
    if(len(user_genre) > 0):
        for index, row in final_movie_data.iterrows():
            movie_id = 0
            movie_id_list = getMovieIdForMovie(row['recommendedMovie'], data)
            
            if(len(movie_id_list) > 0):
                movie_id = movie_id_list.iloc[0]
                
            movie_genre_data = movies.loc[movies['movieId'] == movie_id]
            
            movie_genre = []
            
            for index, movie in movie_genre_data.iterrows():
                if(movie['genres'] != '(no genres listed)'):
                    split_genres = movie['genres'].split('|')
                    for genre in split_genres:
                        if(genre not in movie_genre):
                            movie_genre.append(genre)
            
            genre_jaccard = jaccard_similarity(user_genre, movie_genre)
            genre_dataframe = {'userId': row['userId'], 'recommendedMovie' : row['recommendedMovie'], 'timeRank' : row['timeRank'], 'tagJaccard': row['tagJaccard'], 'genreJaccard': genre_jaccard}
            recommendation_tag_genre_time = recommendation_tag_genre_time.append(genre_dataframe, ignore_index=True)
            
    else:
        for index, row in final_movie_data.iterrows(): 
            genre_dataframe = {'userId': row['userId'], 'recommendedMovie' : row['recommendedMovie'], 'timeRank' : row['timeRank'], 'tagJaccard': row['tagJaccard'], 'genreJaccard': 0}
            recommendation_tag_genre_time = recommendation_tag_genre_time.append(genre_dataframe, ignore_index=True)

recommendation_tag_genre_time = recommendation_tag_genre_time.sort_values(['userId', 'timeRank'], ascending=[True, True])
recommendation_tag_genre_time.to_csv('recommendation_time.csv')

recommendation_tag_genre_time = recommendation_tag_genre_time.sort_values(['userId', 'tagJaccard', 'timeRank'], ascending=[True, False, True])
recommendation_tag_genre_time.to_csv('recommendation_tag_time.csv')

recommendation_tag_genre_time = recommendation_tag_genre_time.sort_values(['userId', 'genreJaccard', 'tagJaccard', 'timeRank'], ascending=[True, False, False, True])
recommendation_tag_genre_time.to_csv('recommendation_tag_genre_time.csv')